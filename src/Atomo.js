import React from 'react'


class Atomo extends React.Component{
    constructor(props){
      super(props);
      this.state = {i: 0};
      this.incrementa = this.incrementa.bind(this);
      this.cambia = this.cambia.bind(this);

    }


    incrementa(e) {
      console.log(this.state.i);
      this.setState((state, props)=>({i: state.i+1}));
      console.log(this.state.i);
    }

    cambia(e){
      e.persist();
      this.setState((state, props)=>({i: parseInt(e.target.value)}));
      console.log(this.state.i);

    }

    render(){
      return(
       <div>
            <input  value={this.state.i} onChange={this.cambia} />
            <button onClick={this.incrementa} style={{width: 100, height: 30, background: "red"}}>Incrementa</button>

       </div>);
  
  }
}

export default Atomo;
