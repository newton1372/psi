import React from 'react'


class HamiltonianText extends React.Component{
    constructor(props){
      super(props);
      this.changeHamiltonianText = this.changeHamiltonianText.bind(this);

    }

    changeHamiltonianText(e){
      e.persist();
      this.props.callbackHamiltonian(e.target.value);
    

    }
  

    render(){
      return(
       <div>
         <label>Hamiltonian of the system: </label><br/>
          <input type="text" value={this.props.Hamiltonian} onChange = {this.changeHamiltonianText}/>
       </div>);
  
  }
}

export default HamiltonianText;
