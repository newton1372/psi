import React from 'react'


class Psi0Text extends React.Component{
    constructor(props){
      super(props);
      this.changePsi0Text = this.changePsi0Text.bind(this);

    }

    changePsi0Text(e){
      e.persist();
      this.props.callbackPsi0(e.target.value);
    

    }
  

    render(){
      return(
       <div>
         <label>Wave function at t=0</label><br/>
          <input type="text" value={this.props.Psi0} onChange = {this.changePsi0Text}/>

       </div>);
  
  }
}

export default Psi0Text;
