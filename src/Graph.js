import React from 'react'
import * as THREE from 'three' 
import * as THREEORB from "three/examples/jsm/controls/OrbitControls"

class Graph extends React.Component{
    constructor(props){
      super(props);
      this.scene= new THREE.Scene();
      this.camera= new THREE.PerspectiveCamera(45, 1, 3, 1000 );
      this.renderer = new THREE.WebGLRenderer();
      
      }
    

    

      
    manageSceneCameraRenderer = ()=>{
      this.scene.background = new THREE.Color("white");
      this.camera.position.set(4,4,20);
     // this.camera.aspect = this.sizes.width/this.sizes.height;
      this.renderer.setSize(this.sizes.width, this.sizes.height);

      var axis = new THREE.AxesHelper(5);
      this.scene.add(axis);

    }


    componentDidMount= ()=>{
    
      this.sizes = {
        width: document.getElementById("figHere").getAttribute("width"),
        height: document.getElementById("figHere").getAttribute("height")
      }
      this.manageSceneCameraRenderer();

      document.getElementById("figHere").appendChild(this.renderer.domElement);
      var controls = new THREEORB.OrbitControls(this.camera, this.renderer.domElement);
      controls.movementSpeed = 10;
      controls.lookSpeed = 1;
      controls.rollSpeed = 0;
      controls.autoForward = false;
            //build points
            var row = [];
            for(var i = -1; i<=5; i+=0.5){
              for(var j =-1; j<=5; j+=0.5){
                for(var k =-1; k<=5; k+=0.5){
                  var pointPosition = new THREE.Vector3(i,j,k)
                  var pointGeometry = new THREE.SphereGeometry(0.05);
                  var pointMaterial = new THREE.MeshBasicMaterial({color: new THREE.Color("black")});
                  var point = new THREE.Mesh(pointGeometry,pointMaterial);
                  point.position.set(pointPosition.x, pointPosition.y, pointPosition.z);
                  row.push(point);
                }
              }
            }

       var scene = this.scene;
       var camera = this.camera;
       var renderer = this.renderer;
      row.map(p => {
        console.log(p);
        scene.add(p);
      });

      var animate = function () {
        requestAnimationFrame( animate );
        controls.update()
        renderer.render(scene, camera );
      };
      animate();
    }


    render(){
     
      return(
       <div  id="figHere" style={{borderStyle:"solid"}} width={600} height={400}> 
      
       </div>
  
      );
    }
}

export default Graph;
