import React from 'react'
import Menu from './Menu'
import Graph from './Graph'
import HamiltonianText from './HamiltonianText'
import Psi0Text from './Psi0Text'
import FindEigenstates from './FindEigenstates'
import Animate from './Animate'


class Central extends React.Component{
    constructor(props){
      super(props);
      this.state={Hamiltonian: "p^2/2m", Psi0: "exp(-r/2a)", Animate: false}
    }


    updateHamiltonian = (HamiltonianFromHamiltonianText) =>{
      console.log(HamiltonianFromHamiltonianText);
      this.setState((state,props)=>({
        Hamiltonian: HamiltonianFromHamiltonianText
      })  
    )}

    updatePsi0 = (Psi0FromPsi0Text) =>{
      this.setState((state,props)=>({
      Psi0: Psi0FromPsi0Text
      })  
    )}
    
    updateAnimate = (newAnim) =>{
      this.setState((state,props)=>({
      Animate: newAnim
      })  
    )}
  

    render(){
      return(
       <div>
         <table cellSpacing={10}>  
          <tr>
            <td Colspan={3} ><Menu/></td>
          </tr>
          <tr>
            <td Colspan={3} />
          </tr>
          <tr>
            <td  Rowspan={4} id="graph">
              <Graph/>
            </td>
            <td Colspan={2}>
              <HamiltonianText callbackHamiltonian={this.updateHamiltonian} Hamiltonian={this.state.Hamiltonian}/>
            </td>
          </tr>
          <tr>
            <td Colspan={2}>
              <Psi0Text callbackPsi0={this.updatePsi0} Psi0={this.state.Psi0}/>
            </td>         
          </tr>
          <tr>
            <td Colspan={3}/>
          </tr>
          <tr>
            <td>         
              <FindEigenstates Hamiltonian={this.state.Hamiltonian}/>
            </td>
            <td>
              <Animate Hamiltonian={this.state.Hamiltonian} Psi0={this.state.Psi0} Animate = {this.state.Animate}/>
            </td>
           </tr>
      


         </table>

       </div>);
  
  }
}

export default Central;
