import React from 'react'


class Menu extends React.Component{
    constructor(props){
      super(props);
    }

    render(){
      return(
       <div>
           <table border="1" style={{width: "100%", border:"1px", background: "grey", colour: "black", fontfamily:"TimesNewRoman",fontsize:"13px"}}>
            <tr> 
              <td>New simulation</td>
              <td>Save eigenstates as png</td>
              <td>Save animation</td>
              <td>Settings</td>
              <td>Help</td></tr>
           </table>
          <br/> <br/> <br/>
       </div>);
  }
}

export default Menu;
